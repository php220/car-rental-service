<?php

namespace App\Models;

use App\Enums\CarStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "cars";

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = ["brand", "model", "type", "box_type", "fuel_type", "seating", "price_per_day", "status"];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'status' => CarStatusEnum::class
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'car_id', 'id');
    }

}
