<?php

namespace App\Enums;

enum CarStatusEnum:string {
    case Active = 'active';
    case Locked = 'locked';
}
