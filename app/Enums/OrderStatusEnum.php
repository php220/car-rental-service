<?php

namespace App\Enums;

enum OrderStatusEnum:string {
    case New = 'new';
    case Approve = 'approve';
    case Cancel = 'cancel';
}
